package dev.liang.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dev.liang.models.Accounts;
import dev.liang.util.ConnectionUtil;

public class AccountsDao implements Dao<Accounts> {

	// the userID is the parameter because accountnumber is a foreign key
	// referencing the userID
	@Override
	public Accounts get(int userID) {
		String sql = "select a_id, accountnumber, balance from accountbalance where accountnumber = ?";
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setInt(1, userID);
			ResultSet rs = ps.executeQuery();
			Accounts accounts = new Accounts();
			while (rs.next()) {
				accounts.setId(rs.getInt("a_id"));
				accounts.setAccountNumber(rs.getInt("accountnumber"));
				accounts.setBalance(rs.getFloat("balance"));
			}
			return accounts;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return null;

	}

	@Override
	public List<Accounts> getAll() {
		String sql = "select a_id, accountnumber, balance from accountbalance";
		try (Connection connection = ConnectionUtil.getConnection();
				Statement s = connection.createStatement();
				ResultSet rs = s.executeQuery(sql);) {
			List<Accounts> allAccounts = new ArrayList<>();
			while (rs.next()) {
				Accounts accounts = new Accounts();
				accounts.setId(rs.getInt("a_id"));
				accounts.setAccountNumber(rs.getInt("accountnumber"));
				accounts.setBalance(rs.getFloat("balance"));
				allAccounts.add(accounts);
			}
			return allAccounts;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return null;
	}

	@Override
	public void save(Accounts t) {
		String sql = "insert into accountbalance values (default, ?,?)";
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setInt(1, t.getAccountNumber());
			ps.setFloat(2, 0);
			ps.executeUpdate();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}

	}

	@Override
	public void update(Accounts t, String[] params) {
		// TODO Auto-generated method stub

	}

	public void update(Accounts t, float change) {
		String sql = "update accountbalance set balance = balance + ? where a_id = ?";
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setFloat(1, change);
			ps.setInt(2, t.getId());
			ps.executeUpdate();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
	}

	@Override
	public void delete(Accounts t) {
		// TODO Auto-generated method stub

	}
}
