package dev.liang.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import dev.liang.models.UserCredentials;
import dev.liang.util.ConnectionUtil;

public class UserCredentialsDao implements Dao<UserCredentials> {

	@Override
	public UserCredentials get(int id) {
		String sql = "select id, username, word from usercredentials";
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery(sql);
			UserCredentials user = new UserCredentials();
			while (rs.next()) {
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("word").toCharArray());
			}
			return user;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return null;
	}

	public UserCredentials get(String username) {
		String sql = "select id, username, word from usercredentials where username = ?";
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
			UserCredentials user = new UserCredentials();
			while (rs.next()) {
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("word").toCharArray());
			}
			return user;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return null;
	}

	@Override
	public List<UserCredentials> getAll() {
		String sql = "select id, username, word from usercredentials";
		try (Connection connection = ConnectionUtil.getConnection();
				Statement s = connection.createStatement();
				ResultSet rs = s.executeQuery(sql);) {
			List<UserCredentials> allUsers = new ArrayList<>();
			while (rs.next()) {
				UserCredentials user = new UserCredentials();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("word").toCharArray());
				allUsers.add(user);
			}
			return allUsers;
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		return null;
	}

	@Override
	public void save(UserCredentials t) {
		String sql = "insert into usercredentials values (default, ?,?)";
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement ps = connection.prepareStatement(sql);) {
			ps.setString(1, t.getUsername());
			ps.setString(2, String.valueOf(t.getPassword()));
			ps.executeUpdate();
		}

		catch (SQLException throwables) {
			throwables.printStackTrace();
		}

	}

	@Override
	public void update(UserCredentials t, String[] params) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(UserCredentials t) {
		// TODO Auto-generated method stub

	}

}
