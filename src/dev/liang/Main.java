package dev.liang;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

import dev.liang.daos.AccountsDao;
import dev.liang.daos.UserCredentialsDao;
import dev.liang.models.Accounts;
import dev.liang.models.UserCredentials;

public class Main {
	private static UserCredentials user = new UserCredentials();
	private static Accounts account = new Accounts();
	private static Scanner scanner = new Scanner(System.in);
	private static int userChoice = 0;

	// Intro Menu
	public static void main(String[] args) {
		boolean exit = false;
		while (!exit) {
			System.out.println("Welcome to the Bank of Money!\n" + "1. Register a user account\n"
					+ "2. Login to your existing user account\n" + "3. Exit");
			userChoice();
			switch (userChoice) {
			case 1:
				userCreation();
				break;
			case 2:
				userMenu();
				break;
			case 3:
				exit = true;
				break;
			default:
				System.out.println("Please pick a valid option");
				break;
			}
		}
		scanner.close();
		System.out.println("Goodbye!");
	}

	// Menu for User Login and Bank Account Creation
	public static void userMenu() {
		String username = null;
		String password = null;
		boolean userExit = false;
		boolean userExit2 = false;
		UserCredentialsDao userDao = new UserCredentialsDao();
		while (!userExit) {
			System.out.println("Enter your username");
			username = scanner.nextLine();
			if (userDao.get(username).getId() == 0) {
				System.out.println("Username not found");
				return;
			}
			user = userDao.get(username);
			System.out.println("Enter your password");
			password = scanner.nextLine();
			if (!Arrays.equals(user.getPassword(), password.toCharArray())) {
				System.out.println("Wrong password");
				return;
			} else {
				System.out.println("You have logged in");
				userExit = true;
			}
		}
		while (!userExit2) {
			System.out.println(
					"1. Create a checking account\n" + "2. Enter an existing checking account\n" + "3. Log out");
			userChoice = 0;
			userChoice();
			switch (userChoice) {
			case 1:
				accountCreation(user);
				break;
			case 2:
				accountMenu(user);
				break;
			case 3:
				userExit2 = true;
				break;
			default:
				System.out.println("Please pick a valid option");
				break;
			}
		}

	}
	
	// Menu for Viewing Bank Account Balance and Deposit/Withdrawal
		public static void accountMenu(UserCredentials user) {
			boolean accountExit = false;
			float balance = 0;
			float value = 0;

			AccountsDao accountsDao = new AccountsDao();
			if (accountsDao.get(user.getId()).getId() == 0) {
				System.out.println("No account");
				return;
			}
			account = accountsDao.get(user.getId());
			while (!accountExit) {
				account = accountsDao.get(user.getId());
				balance = account.getBalance();
				System.out.print("Your current balance is $");
				System.out.printf("%.2f", balance);
				System.out.println("\n" + "1. Deposit funds\n"
							+ "2. Withdraw funds\n" 
							+ "3. Log Out");
				userChoice = 0;
				value = 0;
				userChoice();
				switch (userChoice) {
				case 1:
					System.out.println("Please enter the amount you wish to deposit");
					value = userChoiceFloat();
					accountsDao.update(account, value);
					break;
				case 2:
					System.out.println("Please enter the amount you wish to withdraw");
					value = userChoiceFloat();
					if (account.getBalance() < value) {
						System.out.println("Insufficient funds");
					} else {
						accountsDao.update(account, -value);
					}
					break;
				case 3:
					accountExit = true;
					break;
				default:
					System.out.println("Please pick a valid option");
					break;
				}
			}
		}

	// Method for User Account Creation
	public static void userCreation() {
		String username;
		UserCredentialsDao userDao = new UserCredentialsDao();
		boolean userCreationExit = false;
		while (!userCreationExit) {
			System.out.println("Please enter a username");
			username = scanner.nextLine();
			if (!(userDao.get(username).getId() == 0)) {
				System.out.println("Username already taken");
				return;
			}
			user.setUsername(username);
			System.out.println("Please enter a password");
			user.setPassword(scanner.nextLine().toCharArray());
			userDao.save(user);
			userCreationExit = true;
			System.out.println("User account created");
		}
	}

	// Method for Account Creation
	public static void accountCreation(UserCredentials user) {
		AccountsDao accountsDao = new AccountsDao();
		if (accountsDao.get(user.getId()).getId() == 0) {
			account.setAccountNumber(user.getId());
			accountsDao.save(account);
			System.out.println("Account Created");
		} else {
			System.out.println("You already own a checking account");
		}
	}

	// Method for InputMismatchException on User Input
	public static void userChoice() {
		do {
			try {
				userChoice = scanner.nextInt();
			} catch (InputMismatchException ime) {
				System.out.println("Invalid Input");
			}
			scanner.nextLine(); // clears the buffer
		} while (userChoice <= 0);
	}

	public static float userChoiceFloat() {
		float f = 0;
		do {
			try {
				f = scanner.nextFloat();
			} catch (InputMismatchException ime) {
				System.out.println("Invalid Input");
			}
			scanner.nextLine(); // clears the buffer
		} while (f <= 0);
		return f;
	}
}
