package dev.liang.models;

public class Accounts {
	private int id;
	private int accountNumber;
	private float balance;

	public Accounts() {
		super();
	}

	public Accounts(int id, int accountNumber, float balance) {
		super();
		this.id = id;
		this.accountNumber = accountNumber;
		this.balance = balance;
	}

	public int getId() {
		return this.id;
	}

	public int getAccountNumber() {
		return this.accountNumber;
	}

	public float getBalance() {
		return this.balance;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setAccountNumber(int accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}
}
