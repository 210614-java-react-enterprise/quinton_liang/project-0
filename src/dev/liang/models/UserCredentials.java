package dev.liang.models;

public class UserCredentials {
	private int id;
	private String username;
	private char[] password;

	public UserCredentials() {
		super();
	}

	public UserCredentials(int id, String username, char[] password) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
	}

	public int getId() {
		return this.id;
	}

	public String getUsername() {
		return this.username;
	}

	public char[] getPassword() {
		return this.password;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}

}
