package dev.liang.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {

	private static Connection connection;

	public static Connection getConnection() throws SQLException {
		if (connection == null || connection.isClosed()) {
			String url = "jdbc:postgresql://ql-db.c47ly0bdirdc.us-east-2.rds.amazonaws.com:5432/postgres";
			final String USERNAME = System.getenv("USERNAME");
			final String PASSWORD = System.getenv("PASSWORD");

			connection = DriverManager.getConnection(url, USERNAME, PASSWORD);
		}

		return connection;
	}
}
